#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>

#include "vbios-tables.h"

#pragma pack(push, 1)

#define VOLTAGE_TYPE_VDDC				1
#define VOLTAGE_TYPE_MVDDC				2
#define VOLTAGE_TYPE_MVDDQ				3
#define VOLTAGE_TYPE_VDDCI				4
#define VOLTAGE_TYPE_VDDGFX				5

#define	VOLTAGE_MODE_GPIO_LUT			0
#define VOLTAGE_MODE_INIT_REGULATOR		3
#define VOLTAGE_MODE_VOLTAGE_PHASE		4
#define VOLTAGE_MODE_SVID2				7
#define VOLTAGE_MODE_EVV				8

typedef struct _VOIEntryHeader
{
	uint8_t VoltageType;
	uint8_t VoltageMode;
	uint16_t Size;
} VOIEntryHeader;

typedef struct _VOIEntryType3
{
	uint8_t RegulatorID;
	uint8_t I2CLine;
	uint8_t I2CAddress;
	uint8_t ControlOffset;
	uint8_t VoltageControlFlag;
	uint8_t Reserved[3];
} VOIEntryType3;

typedef struct _VoltageLUTEntry
{
	uint32_t VoltageID;
	uint16_t VoltageValue;
} VoltageLUTEntry;

typedef struct _VOIEntryType0
{
	uint8_t VoltageGPIOCntlID;
	uint8_t GPIOEntryNum;
	uint8_t PhaseDelay;
	uint8_t Reserved;
	uint32_t GPIOMaskValue;
} VOIEntryType0;

typedef struct _VOIEntry
{
	VOIEntryHeader Hdr;
	union
	{
		VOIEntryType0 AsType0;
		VOIEntryType3 AsType3;
	};
} VOIEntry;

// The VO should point INTO the VBIOS buffer
typedef struct _VOListNode
{
	VOIEntry *VO;
	struct _VOListNode *prev;
	struct _VOListNode *next;
} VOListNode;

#pragma pack(pop)

#define VBIOS_GET_PADDING_END(Image)		((uint32_t)((((uint8_t *)(Image))[0x02])) * 512UL)
#define VBIOS_OFFSET(Image, OffsetValue)	(((uint8_t *)Image) + (OffsetValue))
#define VBIOS_GET_ROM_HDR_OFFSET(Image)		(*((uint16_t *)(VBIOS_OFFSET((Image), OFFSET_TO_POINTER_TO_ATOM_ROM_HEADER))))

// Unofficial, but 1MiB had BETTER be enough.
#define VBIOS_MAX_SIZE						0x100000UL

uint32_t VBIOSGetPaddingLength(void *VBIOSImage)
{
	uint32_t PaddingLen = 0;

	// Remember the EndOffset would point to the first byte of the UEFI image if we didn't subtract one.
	for(uint32_t EndOffset = (((uint8_t *)VBIOSImage)[0x03] << 9) - 1; ((uint8_t *)VBIOSImage)[EndOffset - PaddingLen] == 0xFF; PaddingLen++);

	return(PaddingLen);
}

// Beginning at the padding (UEFI image start minus the modification size),
// copy all bytes up by ModLength bytes, creating empty space at the
// offset specified by ModificationOffset that is ModLength bytes in size.
// Does NOT fix ANY length/size entries!
void PrepareROMForInsertionMod(void *VBIOSImage, size_t PadOffset, size_t ModificationOffset, int32_t ModLength)
{
	//for(int i = PadOffset; i >= ModificationOffset; --i) VBIOSImage[i + ModLength] = VBIOSImage[i];
	//for(int i = ModificationOffset; i < PadOffset; ++i) VBIOSImage[i] = VBIOSImage[i + ModLength];

	memmove(((uint8_t *)VBIOSImage) + ModificationOffset + ModLength, ((uint8_t *)VBIOSImage) + ModificationOffset, PadOffset - ModificationOffset);
}

// WARNING: This code recklessly truncates 512 bytes from the end of
// the VBIOS image as a whole!
void VBIOSExtendLegacyROMImage(void *VBIOSImage, size_t ImageSize)
{
	size_t ModOffset = ((uint8_t *)VBIOSImage)[0x03] << 9;

	PrepareROMForInsertionMod(VBIOSImage, ImageSize - 512UL, ModOffset, 512UL);

	// Update size indicator
	((uint8_t *)VBIOSImage)[0x03]++;

	// Ensure the newly allocated space is marked as padding
	memset(((uint8_t *)VBIOSImage) + ModOffset, 0xFF, 512UL);
}

void FixTableOffsets(void *VBIOSImage, ATOM_ROM_HEADER *VBIOSROMHdr, uint32_t ChangeStartOffset, int32_t ChangeSize)
{
	uint16_t *VBIOSTableEntry;
	uint32_t DataTableSize, CommandTableSize;
	ATOM_MASTER_DATA_TABLE *MasterDataTable = (ATOM_MASTER_DATA_TABLE *)(VBIOS_OFFSET(VBIOSImage, VBIOSROMHdr->usMasterDataTableOffset));
	ATOM_MASTER_COMMAND_TABLE *MasterCommandTable = (ATOM_MASTER_COMMAND_TABLE *)(VBIOS_OFFSET(VBIOSImage, VBIOSROMHdr->usMasterCommandTableOffset));

	// Since every entry is two bytes, size divided by 2 is all entries
	DataTableSize = MasterDataTable->sHeader.usStructureSize;
	CommandTableSize = MasterCommandTable->sHeader.usStructureSize;

	VBIOSTableEntry = (uint16_t *)(&MasterDataTable->ListOfDataTables);
	for(int i = 0; i < (DataTableSize >> 1); ++i) VBIOSTableEntry[i] += ((VBIOSTableEntry[i] > ChangeStartOffset) ?  ChangeSize : 0);

	VBIOSTableEntry = (uint16_t *)(&MasterCommandTable->ListOfCommandTables);
	for(int i = 0; i < (CommandTableSize >> 1); ++i) VBIOSTableEntry[i] += ((VBIOSTableEntry[i] > ChangeStartOffset) ?  ChangeSize : 0);
}

// TODO/FIXME: Check Content & Format revisions of the VOI table passed by caller
// TODO: Maybe allow callet to specify type and/or mode at some point?
uint16_t GetVOList(VOListNode **OutputList, uint8_t *VOITableBase)
{
	uint16_t TableSize, CurOffset, EntriesFound = 0;

	if(!OutputList || !VOITableBase) return(0);

	TableSize = (((ATOM_COMMON_TABLE_HEADER*)VOITableBase)->usStructureSize) - sizeof(ATOM_COMMON_TABLE_HEADER);
	CurOffset = sizeof(ATOM_COMMON_TABLE_HEADER);
	*OutputList = NULL;

	while(CurOffset < TableSize)
	{
		VOIEntry *CurVO = (VOIEntry *)(VOITableBase + CurOffset);

		if(CurVO->Hdr.VoltageMode == VOLTAGE_MODE_INIT_REGULATOR)
		{
			VOListNode *CurrentNode, *NewNode;

			if(!(*OutputList)) *OutputList = CurrentNode = NewNode = (VOListNode *)calloc(1, sizeof(VOListNode));
			else
			{
				for(CurrentNode = *OutputList; CurrentNode->next; CurrentNode = CurrentNode->next);
				NewNode = CurrentNode->next = (VOListNode *)calloc(1, sizeof(VOListNode));
				NewNode->prev = CurrentNode;
			}

			NewNode->VO = CurVO;
			NewNode->next = NULL;
			EntriesFound++;
		}

		CurOffset += CurVO->Hdr.Size;
	}
	return(EntriesFound);
}

// 31 char max + NULL
void PreProcessInput(char *Input)
{
	for(int i = 0; (i < 64) && Input[i]; ++i)
	{
		Input[i] = toupper(Input[i]);
	}
}

// Parameter len is bytes in rawstr, therefore, asciistr must have
// at least (len << 1) + 1 bytes allocated, the last for the NULL
void BinaryToASCIIHex(char *restrict asciistr, const void *restrict rawstr, size_t len)
{
	for(int i = 0, j = 0; i < len; ++i)
	{
		asciistr[j++] = "0123456789abcdef"[((uint8_t *)rawstr)[i] >> 4];
		asciistr[j++] = "0123456789abcdef"[((uint8_t *)rawstr)[i] & 0x0F];
	}

	asciistr[len << 1] = 0x00;
}

// Parameter len is the size in bytes of asciistr, meaning rawstr
// must have (len >> 1) bytes allocated
// Maybe asciistr just NULL terminated?
// Returns length of rawstr in bytes
int ASCIIHexToBinary(void *restrict rawstr, const char *restrict asciistr, size_t len)
{
	for(int i = 0, j = 0; i < len; ++i)
	{
		char tmp = asciistr[i];
		if(tmp < 'A') tmp -= '0';
		else if(tmp < 'a') tmp = (tmp - 'A') + 10;
		else tmp = (tmp - 'a') + 10;

		if(i & 1) ((uint8_t *)rawstr)[j++] |= tmp & 0x0F;
		else ((uint8_t *)rawstr)[j] = tmp << 4;
	}

	return(len >> 1);
}

int32_t PromptForVOEntry(VOIEntry *DefaultTemplate, uint8_t *NewI2CInfo)
{
	char Input[2048];

	printf("Set Voltage Object Fields\n");
	printf("Entering 'h' or 'help' will print information\n");
	printf("Pressing enter without any input will set the default (shown in square brackets.)\n\n");

	printf("Voltage Mode (fixed at VOLTAGE_MODE_INIT_REGULATOR): %d\n", VOLTAGE_MODE_INIT_REGULATOR);

	fflush(stdin);

	do
	{
		printf("Voltage Type [%d]: ", DefaultTemplate->Hdr.VoltageType);
		fgets(Input, 64, stdin);

		PreProcessInput(Input);

		if((!strcmp(Input, "HELP")) || (!strcmp(Input, "H")))
		{
			printf("\nThe Voltage Type indicates which voltage the object refers to.\n");
			printf("1. VDDC\n2. MVDDC\n3. MVDDQ\n4. VDDCI\n5. VDDGFX\n\n");
			continue;
		}

		if(!strcmp(Input, "\n")) break;

		DefaultTemplate->Hdr.VoltageType = strtoul(Input, NULL, 10);

		if(((DefaultTemplate->Hdr.VoltageType) && (DefaultTemplate->Hdr.VoltageType > 5)))
		{
			printf("Invalid input '%s'. Type 'h' for help.\n", Input);
			continue;
		}

	} while(0);

	do
	{
		printf("Regulator ID [%d]: ", DefaultTemplate->AsType3.RegulatorID);
		fgets(Input, 64, stdin);

		PreProcessInput(Input);

		if((!strcmp(Input, "HELP")) || (!strcmp(Input, "H")))
		{
			printf("\nRegulator ID is a number identifying the VRM controller.\n");
			continue;
		}

		if(!strcmp(Input, "\n")) break;

		DefaultTemplate->AsType3.RegulatorID = strtoul(Input, NULL, 10);

		if(((!DefaultTemplate->AsType3.RegulatorID) && (errno == EINVAL)))
		{
			printf("Invalid input '%s'. Type 'h' for help.\n", Input);
			continue;
		}

	} while(0);

	do
	{
		printf("I2C Line [%d]: ", DefaultTemplate->AsType3.I2CLine);
		fgets(Input, 64, stdin);

		PreProcessInput(Input);

		if((!strcmp(Input, "HELP")) || (!strcmp(Input, "H")))
		{
			printf("\nNumeric identifier for the relevant I2C line.\n");
			printf("May be any integer from 0 - 255 (inclusive.)\n");
			continue;
		}

		if(!strcmp(Input, "\n")) break;

		DefaultTemplate->AsType3.I2CLine = strtoul(Input, NULL, 10);

		if(((!DefaultTemplate->AsType3.I2CLine) && (errno == EINVAL)))
		{
			printf("Invalid input '%s'. Type 'h' for help.\n", Input);
			continue;
		}

	} while(0);

	do
	{
		printf("I2C Address [%d]: ", DefaultTemplate->AsType3.I2CAddress);
		fgets(Input, 64, stdin);

		PreProcessInput(Input);

		if((!strcmp(Input, "HELP")) || (!strcmp(Input, "H")))
		{
			printf("\nI2C address in 8-bit format (LSB set to 0.)\n");
			printf("May be any integer from 3 - 119 (inclusive.)\n");
			continue;
		}

		if(!strcmp(Input, "\n")) break;

		DefaultTemplate->AsType3.I2CAddress = strtoul(Input, NULL, 10);

		if(((DefaultTemplate->AsType3.I2CAddress < 3) || (DefaultTemplate->AsType3.I2CAddress > 119)))
		{
			printf("Invalid input '%s'. Type 'h' for help.\n", Input);
			continue;
		}

	} while(0);

	do
	{
		printf("Control Offset [%d]: ", DefaultTemplate->AsType3.ControlOffset);
		fgets(Input, 64, stdin);

		PreProcessInput(Input);

		if((!strcmp(Input, "HELP")) || (!strcmp(Input, "H")))
		{
			printf("\nI'll be honest - I don't know what this is for.\n");
			printf("May be any integer from 0 - 255 (inclusive.)\n");
			continue;
		}

		DefaultTemplate->AsType3.ControlOffset = strtoul(Input, NULL, 10);

		if(((!strcmp(Input, "\n")) && (!DefaultTemplate->AsType3.ControlOffset)) && (errno == EINVAL))
		{
			printf("Invalid input '%s'. Type 'h' for help.\n", Input);
			continue;
		}

	} while(0);

	do
	{
		printf("Voltage Control Flag [%d]: ", DefaultTemplate->AsType3.VoltageControlFlag);
		fgets(Input, 64, stdin);

		PreProcessInput(Input);

		if((!strcmp(Input, "HELP")) || (!strcmp(Input, "H")))
		{
			printf("\nSet to zero, this indicates the I2C data operates on single bytes.\n");
			printf("If set to 1, the I2C transactions will be 2-byte words. May be only zero or one.\n");
			continue;
		}

		DefaultTemplate->AsType3.VoltageControlFlag = strtoul(Input, NULL, 10);

		if(((!strcmp(Input, "\n")) && (!DefaultTemplate->AsType3.VoltageControlFlag)) && (errno == EINVAL))
		{
			printf("Invalid input '%s'. Type 'h' for help.\n", Input);
			continue;
		}

	} while(0);

	do
	{
		uint8_t I2CTmpBuf[1024];
		uint32_t BufSize = ((64 > (DefaultTemplate->Hdr.Size - 12)) ? 64 : (DefaultTemplate->Hdr.Size - 12));
		char *I2CInfo = (char *)malloc(sizeof(char) * ((BufSize << 1) + 1));

		BinaryToASCIIHex(I2CInfo, ((uint8_t *)DefaultTemplate) + 12, (DefaultTemplate->Hdr.Size - 12));
		printf("I2C Info [%s]: ", I2CInfo);
		fgets(Input, 1024, stdin);

		PreProcessInput(Input);

		if((!strcmp(Input, "HELP")) || (!strcmp(Input, "H")))
		{
			printf("\nI2C info for the regulator which this voltage object refers to.\n");
			printf("If you don't know what to put here, you probably should.\n");
			continue;
		}

		if(!strcmp(Input, "\n")) break;

		int32_t NewI2CInfoLen = ASCIIHexToBinary(I2CTmpBuf, Input, strlen(Input));

		if(NewI2CInfoLen > (DefaultTemplate->Hdr.Size - 12))
		{
			int32_t RemainingBytes = (int32_t)(NewI2CInfoLen - (DefaultTemplate->Hdr.Size - 12));
			memcpy(((uint8_t *)DefaultTemplate) + 12, I2CTmpBuf, (DefaultTemplate->Hdr.Size - 12));
			memcpy(NewI2CInfo, I2CTmpBuf + (DefaultTemplate->Hdr.Size - 12), RemainingBytes);
			return(RemainingBytes);
		}
		else if(NewI2CInfoLen < (DefaultTemplate->Hdr.Size - 12))
		{
			memcpy(((uint8_t *)DefaultTemplate) + 12, I2CTmpBuf, NewI2CInfoLen);
			int32_t RemainingBytes = (int32_t)(NewI2CInfoLen - (((int32_t)DefaultTemplate->Hdr.Size) - 12));
			return(RemainingBytes);
		}
		memcpy(((uint8_t *)DefaultTemplate) + 12, I2CTmpBuf, NewI2CInfoLen);

	} while(0);

	return(0);
}

int main(int argc, char **argv)
{
	VOIEntry VO;
	size_t BytesRead;
	void *VBIOSImageData;
	FILE *VBIOSFileHandle;
	ATOM_ROM_HEADER *VBIOSROMHdr;
	VOListNode *VOList, *CurNode;
	ATOM_COMMON_TABLE_HEADER *VOIDataTableHdr;
	ATOM_MASTER_LIST_OF_DATA_TABLES *VBIOSDataTableList;
	uint32_t PaddingLen = 0, PaddingStartOffset = 0, ModOffset = 0;

	if(argc < 2)
	{
		printf("Usage: %s <VBIOS file>\n", argv[0]);
		return(1);
	}

	VBIOSFileHandle = fopen(argv[1], "rb+");

	if(!VBIOSFileHandle)
	{
		printf("Unable to open %s (does it exist?)\n", argv[1]);
		return(-1);
	}

	VBIOSImageData = malloc(VBIOS_MAX_SIZE);

	BytesRead = fread(VBIOSImageData, 1, VBIOS_MAX_SIZE, VBIOSFileHandle);

	// Ensure the read succeeded. The fread function should return
	// the amount read on success. If it's not, it could have been
	// shorter (this is okay) or an error (this is not okay.)
	if((BytesRead != VBIOS_MAX_SIZE) && !(feof(VBIOSFileHandle)))
	{
		printf("Reading the VBIOS file failed.\n");
		fclose(VBIOSFileHandle);
		free(VBIOSImageData);
		return(-2);
	}

	// Checks that the amount of bytes we read is a multiple of
	// 512 - it should be, and if not, we likely have a problem.
	if(BytesRead & 0x1FF)
	{
		printf("Invalid VBIOS size.\n");
		fclose(VBIOSFileHandle);
		free(VBIOSImageData);
		return(-3);
	}

	VBIOSROMHdr = (ATOM_ROM_HEADER *)(VBIOS_OFFSET(VBIOSImageData, VBIOS_GET_ROM_HDR_OFFSET(VBIOSImageData)));

	printf("ROM Table Format Revision 0x%02X, Content Revision 0x%02X.\n", VBIOSROMHdr->sHeader.ucTableFormatRevision, VBIOSROMHdr->sHeader.ucTableContentRevision);

	// The Data Table List's offset itself is not exactly pointed to in the structure which is pointed to in the ROM header...
	// It is actually a member of that structure, as shown below - we use & to take the address of the area in the structure which IS the list.
	VBIOSDataTableList = &((ATOM_MASTER_DATA_TABLE *)(VBIOS_OFFSET(VBIOSImageData, VBIOSROMHdr->usMasterDataTableOffset)))->ListOfDataTables;
	VOIDataTableHdr = (ATOM_COMMON_TABLE_HEADER *)(VBIOS_OFFSET(VBIOSImageData, VBIOSDataTableList->VoltageObjectInfo));

	PaddingLen = VBIOSGetPaddingLength(VBIOSImageData);

	if(PaddingLen < sizeof(VOIEntry))
	{
		printf("Insufficient unused space in the VBIOS for insertion.\n");
		printf("Attempting experimental extension of the legacy ROM image.\n");
		VBIOSExtendLegacyROMImage(VBIOSImageData, BytesRead);
	}

	ATOM_COMMON_TABLE_HEADER *VOIHdr = (ATOM_COMMON_TABLE_HEADER *)(VBIOSImageData + VBIOSDataTableList->VoltageObjectInfo);

	printf("VOI Table Format Revision 0x%02X, Content Revision 0x%02X.\n", VOIHdr->ucTableFormatRevision, VOIHdr->ucTableContentRevision);
	printf("Searching VOI table for voltage objects with mode INIT_REGULATOR...\n");

	uint16_t MatchingEntries = GetVOList(&VOList, (uint8_t *)VOIDataTableHdr);
	CurNode = VOList;

	for(uint16_t idx = 0; CurNode && idx < MatchingEntries; ++idx, CurNode = CurNode->next)
	{
		printf("Object %d:\n", idx);
		printf("\tType: %d\n\tMode: %d\n\tSize: 0x%04X\n\n", CurNode->VO->Hdr.VoltageType, CurNode->VO->Hdr.VoltageMode, CurNode->VO->Hdr.Size);

		printf("RegulatorID = %d\n", CurNode->VO->AsType3.RegulatorID);
		printf("I2CLine = %d\n", CurNode->VO->AsType3.I2CLine);
		printf("I2CAddress = %d\n", CurNode->VO->AsType3.I2CAddress);
		printf("ControlOffset = %d\n", CurNode->VO->AsType3.ControlOffset);
		printf("Voltage entries are %d-bit.\n", (CurNode->VO->AsType3.VoltageControlFlag ? 16 : 8));
		printf("\nContents (displayed as raw hex): \n");
		for(int i = 0; i < (CurNode->VO->Hdr.Size - 12); ++i) printf("%02X", ((uint8_t *)CurNode->VO)[i + 12]);
		putchar('\n');
	}

	do
	{
		char InputStr[65];
		uint8_t I2CInfoTmpBuf[1024];

		printf("\nPress 'e' to edit an entry, 'a' to add one, or 'q' to quit: ");

		fflush(stdin);
		fgets(InputStr, 64, stdin);

		PreProcessInput(InputStr);

		if(!strcmp(InputStr, "E\n"))
		{
			if(!MatchingEntries)
			{
				printf("No supported entries to edit!\n");
				continue;
			}

			do
			{
				printf("Enter index of entry to edit, or 'q' to return to the main menu: ");

				fgets(InputStr, 64, stdin);

				PreProcessInput(InputStr);

				if(!strcmp(InputStr, "Q\n")) break;

				if(!strcmp(InputStr, "\n")) continue;

				uint32_t SelectedIdx = strtoul(InputStr, NULL, 10);

				if((!SelectedIdx) && (errno == EINVAL))
				{
					printf("Invalid input '%s'.\n", InputStr);
					continue;
				}

				if(SelectedIdx >= MatchingEntries)
				{
					printf("Entry with index '%d' does not exist.\n", SelectedIdx);
					continue;
				}

				CurNode = VOList;

				for(int idx = 0; idx < SelectedIdx; ++idx) CurNode = CurNode->next;

				int32_t ExtraBytes = PromptForVOEntry(CurNode->VO, I2CInfoTmpBuf);

				if(ExtraBytes > 0)
				{
					size_t ModOffset = (((uint8_t *)CurNode->VO) + (CurNode->VO->Hdr.Size - 12) + 12) - (uint8_t *)VBIOSImageData;
					PrepareROMForInsertionMod(VBIOSImageData, VBIOS_GET_PADDING_END(VBIOSImageData) - ExtraBytes, ModOffset, ExtraBytes);
					memcpy((((uint8_t *)CurNode->VO) + (CurNode->VO->Hdr.Size - 12) + 12), I2CInfoTmpBuf, ExtraBytes);

					// Correct length of Voltage object
					CurNode->VO->Hdr.Size += ExtraBytes;

					// Correct length of entire VOI table header
					VOIHdr->usStructureSize += ExtraBytes;

					// Correct all table offsets for tables (data and command) which were affected
					FixTableOffsets(VBIOSImageData, VBIOSROMHdr, ModOffset, ExtraBytes);
				}
				else if(ExtraBytes < 0)
				{
					size_t ModOffset = (((uint8_t *)CurNode->VO) + (CurNode->VO->Hdr.Size - 12) + 12) - (uint8_t *)VBIOSImageData;
					uint32_t SizeDiff = abs(ExtraBytes);

					// Correct length of Voltage object
					CurNode->VO->Hdr.Size += ExtraBytes;

					// Correct length of entire VOI table header
					VOIHdr->usStructureSize += ExtraBytes;

					PrepareROMForInsertionMod(VBIOSImageData, VBIOS_GET_PADDING_END(VBIOSImageData) - ExtraBytes, ModOffset, ExtraBytes);

					// Correct all table offsets for tables (data and command) which were affected
					FixTableOffsets(VBIOSImageData, VBIOSROMHdr, ModOffset, ExtraBytes);
				}

			} while(0);
		}
		else if(!strcmp(InputStr, "A\n"))
		{
			// Two extra bytes for the 0xFF 0x00 default I2C data
			VOIEntry *NewEntry = (VOIEntry *)calloc(1, sizeof(VOIEntry) + 2);

			// Fields not set have been zeroed during allocation
			NewEntry->Hdr.VoltageType = VOLTAGE_TYPE_VDDC;
			NewEntry->Hdr.VoltageMode = VOLTAGE_MODE_INIT_REGULATOR;
			NewEntry->Hdr.Size = sizeof(VOIEntry) + 2;

			NewEntry->AsType3.RegulatorID = 0x08;
			NewEntry->AsType3.I2CLine = 150;
			NewEntry->AsType3.I2CAddress = 0x10;

			((uint8_t *)NewEntry)[12] = 0xFF;
			((uint8_t *)NewEntry)[13] = 0x00;

			// In this case, since there is no object being replaced,
			// the entire object is going to have to be inserted.
			size_t ExtraBytes = PromptForVOEntry(NewEntry, I2CInfoTmpBuf);
			size_t ModOffset = VBIOSDataTableList->VoltageObjectInfo + sizeof(ATOM_COMMON_TABLE_HEADER);

			PrepareROMForInsertionMod(VBIOSImageData, VBIOS_GET_PADDING_END(VBIOSImageData) - ExtraBytes - sizeof(VOIEntry) - 2, ModOffset, ExtraBytes + sizeof(VOIEntry) + 2);

			// Correct length of Voltage object PRIOR to insertion!
			NewEntry->Hdr.Size += ExtraBytes;

			memcpy(((uint8_t *)VBIOSImageData) + ModOffset, NewEntry, sizeof(VOIEntry) + 2);
			memcpy(((uint8_t *)VBIOSImageData) + ModOffset + sizeof(VOIEntry) + 2, I2CInfoTmpBuf, ExtraBytes);

			// Correct length of entire VOI table header
			VOIHdr->usStructureSize += ExtraBytes + sizeof(VOIEntry) + 2;

			// Correct all table offsets for tables (data and command) which were affected
			FixTableOffsets(VBIOSImageData, VBIOSROMHdr, ModOffset, ExtraBytes + sizeof(VOIEntry) + 2);
			free(NewEntry);
		}
		else if(!strcmp(InputStr, "Q\n"))
		{
			rewind(VBIOSFileHandle);
			fwrite(VBIOSImageData, sizeof(uint8_t), BytesRead, VBIOSFileHandle);
			free(VBIOSImageData);
			fclose(VBIOSFileHandle);
			break;
		}
	} while(1);

	return(0);
}
